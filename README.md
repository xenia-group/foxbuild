# foxbuild
`foxbuild` is the build tool used to create Xenia Linux RootFS images.
With this tool, a user can build a Stage4 archive, then create a SquashFS archive from it, which they can use to boot Xenia Linux.
The tool makes use of Catalyst 4 to build the initial Stage4 archive, alongside `squashfs-tools`` to create the SquashFS archive.

## Building

To build a RootFS image, you can run the following command:

```bash
# Run foxbuild 
sudo foxbuild
```

This starts `foxbuild` in an interactive mode, where a user can specify their build options.

`foxbuild` can also be run using a TOML formatted config file, which can be specified using the `-c` flag:

```bash
# Specify config file
sudo foxbuild -c /path/to/config.toml
```

`foxbuild` contains an update argument for when you need to update the catalyst repo, stage archives, etc...
This update argument simply updates your required files for building during the build process, so you dont need to run with `-u` then run `foxbuild` again.

```bash
# Update foxbuild
sudo foxbuild -u
```

## Config File

The config file should look like this:

```toml
# The git repository for the catalyst files (must be in the same file structure as the one on the link below)
catalyst_repo = "https://gitlab.com/xenia-group/catalyst.git"
# The git branch to use for the catalyst files
git_branch = "unstable"
# The type of stage4 to build (this will be your spec file name without the .spec extension)
target = ['systemd-plasma', 'systemd', 'systemd-arm']
# The number of jobs to use for catalyst
catalyst_jobs = "-j4"
```

### Breaking down the config file

#### `catalyst_repo`

This variable in the config file specifies the repository that will be used to fetch the .spec files from.

For official build images, we use the `catalyst` repo located in Xenia Group on our GitLab, that being located [here](https://gitlab.com/xenia-group/catalyst)

If you are going to use your own catalyst repo, it must be in the same file structure as our repository, the easiest way to do this is by cloning the catalyst repo and editing the files in there.

#### `git_branch`

This variable is quite self-explanatory, it specifies the branch to be used for fetching the spec files in git.

This can be quite useful when testing experimental branches / development branches, such as `unstable` in the official Xenia Linux catalyst repository.

#### `target`

This variable specifies the spec file/s to be used when building the Stage 4 root images. 

You can specify multiple by comma seperating the values.
