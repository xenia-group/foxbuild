import os
import foxcommon as fc
import datetime
import subprocess
import urllib.request
import argparse
import tomllib
import tomli_w
import shutil
import hashlib
import psutil
import catalyst_parser


class Specfile(catalyst_parser.Spec):
    working_dir = "/var/foxbuild"
    mirror = os.getenv("MIRROR", default="https://mirror.bytemark.co.uk/gentoo")

    def get_changed(self):
        os.chdir(f"{self.working_dir}/catalyst")

        self._git_commit = ""

        if os.getenv("CI_MERGE_REQUEST_DIFF_BASE_SHA") != None:
            self._git_commit = os.getenv("CI_MERGE_REQUEST_DIFF_BASE_SHA")
            self._method = "merge_req"
            self._command = f"git diff-tree -r --no-commit-id --name-only {self._git_commit} HEAD" # comparing target branch to current
        elif os.getenv("CI_MERGE_REQUEST_TARGET_BRANCH_SHA") != None:
            self._git_commit = os.getenv("CI_MERGE_REQUEST_TARGET_BRANCH_SHA")
            self._method = "merged_req"
            self._command = f"git diff-tree -r --no-commit-id --name-only {self._git_commit} HEAD" # comparing target branch to current
        elif os.getenv("CI_COMMIT_SHA") != None:
            self._git_commit = os.getenv("CI_COMMIT_SHA")
            self._method = "ci_commit"
            self._command = f"git diff-tree -r --no-commit-id --name-only {self._git_commit} HEAD~1" # latest commit vs one before
        else:
            self._git_commit_cmd = subprocess.run(
                "git rev-parse HEAD~1".split(" "), capture_output=True, text=True
            )
            self._git_commit = self._git_commit_cmd.stdout.strip()
            self._method = "manual"
            self._command = f"git diff-tree -r --no-commit-id --name-only {self._git_commit} HEAD" # comparing target branch to current

        self._changed_files_cmd = subprocess.run(
            self._command.split(" "),
            capture_output=True,
            text=True,
        )

        self._overlays = [overlay.split("/")[-1] for overlay in self.values["stage4/root_overlay"]]
        self._reason = "Not changed."

        for changed in self._changed_files_cmd.stdout.strip().split("\n"):
            if os.path.exists(f"{self.working_dir}/catalyst/{changed.strip()}"):
                if self.name == changed:
                    self.spec_changed = True
                    self._reason = "name/spec"
                    break
                elif self.values["stage4/fsscript"].split("/")[-1] == changed:
                    self.spec_changed = True
                    self._reason = "name/spec"
                    break
                elif changed.startswith(self.values["portage_confdir"].replace("/var/tmp/catalyst/", "")):
                    self.spec_changed = True
                    self._reason = f"config change: {changed}"
                    break
                else:
                    for overlay in self._overlays:
                        if changed.split("/")[0].startswith(overlay):
                            self.spec_changed = True
                            self._reason = f"overlay change: {overlay}"
                            break

        print(f"git commit: {self._git_commit}\nmethod: {self._method}\nchanged: {self.spec_changed}\ncommand: {self._command}\nwhy: {self._reason}")

    def is_new_stage3(self):
        fc.info(f"Getting hash for {self._stage3_link}")

        with urllib.request.urlopen(f"{self._stage3_link}.sha256") as hash_file:
            for line in hash_file.read().strip().decode().split("\n"):
                if self.target in line:
                    self._new_hash = line.split(" ")[0]

        self._sha256 = hashlib.sha256()

        with open(f"/var/tmp/catalyst/builds/{self.values['source_subpath']}.tar.xz", "rb") as f:
            while True:
                data = f.read(65536)
                if not data:
                    break
                self._sha256.update(data)

        self._old_hash = self._sha256.hexdigest()

        if self._old_hash in self._new_hash:
            return False
        else:
            return True

    def get_stage3_link(self):
        with urllib.request.urlopen(
            f"{self.mirror}/releases/{self.values['subarch']}/autobuilds/latest-{self.target}.txt"
        ) as latest:
            for line in latest.read().strip().decode().split("\n"):
                if self.target in line:
                    self._stage3_file = line.split(" ")[0]
                    self._stage3_link = f"{self.mirror}/releases/{self.values['subarch']}/autobuilds/{self._stage3_file}"

    def get_stage3(self):
        if self._stage3_link == "":
            self.get_stage3_link()

        if os.path.exists(f"/var/tmp/catalyst/builds/{self.values['source_subpath']}.tar.xz"):
            if self.is_new_stage3():
                os.remove(f"/var/tmp/catalyst/builds/{self.values['source_subpath']}.tar.xz")
            else:
                return

        fc.info(f"Downloading stage3: {self._stage3_link}")
        urllib.request.urlretrieve(
            self._stage3_link, f"/var/tmp/catalyst/builds/{self.values['source_subpath']}.tar.xz"
        )

    def setup_spec(self, name, snapshot):
        with open(f"{self.working_dir}/catalyst/{name}", "r") as spec:
            self._contents = spec.read()

        self._contents = self._contents.replace(
            "[CATALYST_DIR]", f"{self.working_dir}/catalyst/"
        )
        self._contents = self._contents.replace(
            "[SNAPSHOT_HASH]", snapshot.snapshot_hash
        )

        # Write the changes to the file
        with open(f"{self.working_dir}/catalyst/{name}", "w") as file:
            file.write(self._contents)

        super().__init__(f"{self.working_dir}/catalyst/{name}")

        if not self.check_packages():
            fc.die("Spec file is not valid.")

        self.target = self.values["source_subpath"].split("/")[-1]

        if not os.path.exists(f"/var/tmp/catalyst/builds/{self.values['source_subpath']}.tar.xz"):
            self.get_stage3()

    def __init__(self, name, snapshot, changed):
        self._stage3_link = ""
        self.changed = changed    # is target set to changed mode?
        self.spec_changed = False # has this spec been changed?
        self.name = name

        self.setup_spec(name, snapshot)

        if changed:
            self.get_changed()


class Build(Specfile):
    working_dir = "/var/foxbuild"

    with open(f"{working_dir}/output/Manifest.toml", "rb") as existing:
        manifest = tomllib.load(existing)

    def __init__(self, name, snapshot, config, changed, verbose):
        self.name = name
        self.config = config

        if verbose:
            self._flags = "-d "
        else:
            self._flags = ""

        super().__init__(name, snapshot, changed)

    def build(self, ci):
        if self.changed and self.spec_changed == False: # if target set to changed mode and spec hasn't changed
            fc.info(f"Target {self.name} has not changed. Skipping build.")
            return

        self._log_location = f"{self.working_dir}/{self.name}.log"

        if ci:
            fc.info(f"Beginning build of {self.name}. Logging to stdout/stderr")
        else:
            fc.info(f"Beginning build of {self.name}. Logging to {self._log_location}")

        with open(self._log_location, "w") as log:
            os.chdir(f"{self.working_dir}/catalyst")
            try:
                subprocess.run(
                    f"catalyst {self._flags}-a -f {self.name}".split(" "),
                    stdout=None if ci else log,
                    stderr=None if ci else log,
                    check=True,
                )
            except subprocess.CalledProcessError:
                fc.warn("An error occured while building. Trying without -a.")

                try:
                    subprocess.run(
                        f"catalyst {self._flags}-f {self.name}".split(" "),
                        stdout=None if ci else log,
                        stderr=None if ci else log,
                        check=True,
                    )
                except subprocess.CalledProcessError:
                    fc.die(
                        f"{self.name} failed to build. Check logs at {self._log_location} for more detail."
                    )

            fc.info(f"Built {self.name}.")
            fc.info(f"Building squashFS for {self.name}.")

            try:
                self._output_name = f"{self.values['version_stamp']}.img"

                self._output_path = f"{self.working_dir}/output/{self.values['subarch']}/{self.config['git_branch']}/"
                os.makedirs(self._output_path, exist_ok=True)

                self._output = f"{self._output_path}{self._output_name}"

                if os.path.exists(self._output):
                    os.remove(self._output)

                subprocess.run(
                    f"mksquashfs /var/tmp/catalyst/tmp/default/stage4-{self.values['subarch']}-{self.values['version_stamp']} {self._output} -comp xz".split(
                        " "
                    ),
                    stdout=None if ci else log,
                    stderr=None if ci else log,
                    check=True,
                )

                if self.values['version_stamp'] not in Build.manifest.keys():
                    Build.manifest[self.values['version_stamp']] = {
                        "name": self.values['version_stamp'],
                        "versions": {},
                        "arch": [],
                    }

                if (
                    self.config["git_branch"]
                    not in Build.manifest[self.values['version_stamp']]["versions"].keys()
                ):
                    Build.manifest[self.values['version_stamp']]["versions"][
                        self.config["git_branch"]
                    ] = {"filename": self._output_name, "arch": []}

                if (
                    self.values['subarch']
                    not in Build.manifest[self.values['version_stamp']]["versions"][
                        self.config["git_branch"]
                    ]["arch"]
                ):
                    Build.manifest[self.values['version_stamp']]["versions"][
                        self.config["git_branch"]
                    ]["arch"].append(self.values['subarch'])

                if self.values['subarch'] not in Build.manifest[self.values['version_stamp']]["arch"]:
                    Build.manifest[self.values['version_stamp']]["arch"].append(self.values['subarch'])

            except subprocess.CalledProcessError:
                fc.die(
                    f"{self.name} failed to create squashFS. Check logs at {self._log_location} for more detail."
                )

        fc.info(f"Built {self.name} to {self._output}")


class Snapshot:
    @property
    def snapshot_path(self):
        return self._snapshot_path

    @snapshot_path.setter
    def snapshot_path(self, value):
        self._snapshot_path = value
        self.snapshot_hash = self._snapshot_path.split("gentoo-")[1].replace(
            ".sqfs", ""
        )
        self.snapshot_date = datetime.datetime.fromtimestamp(
            os.path.getmtime(self._snapshot_path)
        )

    def __init__(self, snapshot_name=""):
        if snapshot_name != "":
            self.snapshot_path = f"/var/tmp/catalyst/snapshots/{snapshot_name}"
        else:
            self.snapshot_path = self.create_snapshot()

    def create_snapshot(self):
        self._snapshot_output = subprocess.run(
            ["catalyst", "-s", "stable"], capture_output=True
        )
        self._snapshot_output = self._snapshot_output.stdout.decode(
            "utf-8"
        ).splitlines()

        return self._snapshot_output[-1].split("Wrote snapshot to ")[-1]


class Environment:
    working_dir = "/var/foxbuild"

    def catalyst_conf(self):
        total_memory = psutil.virtual_memory().total
        threads = psutil.cpu_count(logical=True)

        if total_memory / 2147483648 < threads:  # which is smaller, threads or ram /2GB
            jobs = round(total_memory / 2147483648)
        else:
            jobs = threads

        with open("/etc/catalyst/catalystrc", "r") as f:
            contents = f.read()

        if contents.find("MAKEOPTS") != -1:
            fc.warn(
                f"/etc/catalystrc MAKEOPTS written manually. Not overwriting with recommended jobs value -j{jobs}"
            )
        else:
            contents += f'export MAKEOPTS="-j{jobs}"'

            with open("/etc/catalyst/catalystrc", "w") as f:
                f.write(contents)

    def latest_snapshot(self):
        self._latest = self.snapshots[0]
        for snapshot in self.snapshots:
            if snapshot.snapshot_date > self._latest.snapshot_date:
                self._latest = snapshot

        return self._latest

    def add_build(self, name, snapshot):
        self.build_queue.append(Build(name, snapshot, self.config, self.changed, self._verbose))

    def start_build(self):
        for build in self.build_queue:
            build.build(self.ci)

        with open(f"{self.working_dir}/output/Manifest.toml", "wb") as manifest:
            tomli_w.dump(Build.manifest, manifest)

        fc.info("Built all targets!")

    def _update_repo(self):
        if not os.path.isdir(f"{self.working_dir}/catalyst"):
            fc.info("Cloning catalyst repo.")
            fc.execute(
                f'git clone {self.config["catalyst_repo"]} {self.working_dir}/catalyst'
            )

            fc.info("Symlinking config dir.")
            fc.execute(f"ln -sf {self.working_dir}/catalyst/config /var/tmp/catalyst/")

        fc.info("Updating catalyst repo")
        os.chdir(f"{self.working_dir}/catalyst")
        fc.execute("git reset --hard")
        fc.execute("git fetch")
        fc.execute(f'git checkout {self.config["git_branch"]}')
        fc.execute("git pull")
        os.chdir(f"{self.working_dir}/..")

    def update(self):
        fc.info("Updating xenia-overlay")
        fc.execute("emaint sync -r xenia-overlay")

        if os.path.exists(f"{self.working_dir}/catalyst"):
            shutil.rmtree(f"{self.working_dir}/catalyst")

        self._update_repo()

        fc.info("Generating new Portage snapshot")
        self.snapshots.append(Snapshot())

    def _update_stage3(self):
        fc.info("Updating stage3s")
        for build in self.build_queue:
            build.get_stage3()

    def setup(self):
        self._dirs = [
            "/var/tmp/catalyst/builds/default",
            "/var/cache/distfiles",
            "/var/tmp/catalyst/snapshots/",
            f"{self.working_dir}/output",
        ]

        for dir in self._dirs:
            os.makedirs(dir, exist_ok=True)

        self.catalyst_conf()

        if not os.path.isdir("/var/db/repos/xenia-overlay"):
            fc.info("Installing Xenia overlay.")
            fc.execute(
                "eselect repository add xenia-overlay git https://gitlab.com/xenia-group/xenia-overlay"
            )
            self._update = True
        else:
            fc.info("Xenia overlay already installed, skipping.")

        for snapshot in os.listdir("/var/tmp/catalyst/snapshots/"):
            self.snapshots.append(Snapshot(snapshot))

        if len(self.snapshots) == 0:
            self._update = True

        if self._update == True:
            self.update()
        else:
            self._update_repo()

        if self.config["target"] == "changed":
            self.changed = True

        if self.config["target"] == "*" or self.config["target"] == "changed":
            self.config["target"] = ",".join(
                [
                    target
                    for target in get_targets(self.config)
                    if target not in ["*", "changed"]
                ]
            )

        for target in self.config["target"].split(","):
            self.add_build(target, self.latest_snapshot())

        if self._update == True:
            self._update_stage3()

    def __init__(self, config, args):
        self.build_queue = []
        self.snapshots = []
        self.config = config
        self.ci = args.ci
        self._update = args.update
        self._verbose = args.verbose
        self.changed = False
        self.setup()


def arg_parser() -> str or None:
    """
    Simply parses the command-line arguments for foxbuild.
    This contains an argument for the config file location.
    """

    parser = argparse.ArgumentParser(
        prog="foxbuild",
        description="Official Xenia Linux Build Tool",
        epilog="This tool is part of the Xenia Linux utilities. Find Xenia Linux on GitLab at https://gitlab.com/xenia-group/xenia-linux.",
    )

    parser.add_argument(
        "-c",
        "--config",
        dest="config",
        help="Path to the foxbuild config file.",
        default="",
        action="store",
    )
    parser.add_argument(
        "-u",
        "--update",
        dest="update",
        help="Update the build environment.",
        default=False,
        action="store_true",
    )
    parser.add_argument(
        "-n",
        "--nobuild",
        dest="nobuild",
        help="Don't build, just setup the repo.",
        default=False,
        action="store_true",
    )
    parser.add_argument(
        "-i",
        "--ci",
        dest="ci",
        help="CI mode (redirect log output to stdout/stderr)",
        default=False,
        action="store_true",
    )
    parser.add_argument(
        "-v",
        "--verbose",
        dest="verbose",
        help="Verbose mode (turns on verbose logging in catalyst)",
        default=False,
        action="store_true",
    )

    args = parser.parse_args()

    return args


def toml_parser(args: argparse.Namespace) -> dict:
    """
    Parses the config file for foxbuild.
    This is a TOML file, and is parsed using the toml library.
    """

    if args != "":
        fc.info(f"Using config file {args}.")

        # Check if the config file exists
        if not os.path.isfile(args):
            fc.die(f"Config file {args} does not exist.")

        # Open the config file
        with open(args, "rb") as f:
            data = tomllib.load(f)

    return data


def catalyst_repo(value: str) -> bool:
    """
    Checks that the git repo in the config file exists.
    This returns a boolean.
    """

    # Check if the remote exists
    remote = subprocess.run(
        ["git", "ls-remote", "--exit-code", value],
        stdout=subprocess.DEVNULL,
        stderr=subprocess.DEVNULL,
    ).returncode

    if remote == 0:  # Is remote 0?
        return True
    else:
        return False


def git_branch(ret) -> list:
    """
    Checks the branches in the git remote and returns them as a list.
    """

    result = subprocess.run(
        ["git", "ls-remote", "--heads", ret["catalyst_repo"]],
        stdout=subprocess.PIPE,
        text=True,
    )
    branches = result.stdout.splitlines()

    # Split the branches into a list
    branches = [branch.split("refs/heads/")[1] for branch in branches]

    return branches


def check_target_list(value, ret) -> bool:
    targets = get_targets(ret)

    for target in value.split(","):
        if target not in targets:
            return False

    return True


def get_targets(ret) -> list:
    """
    Gets a directory listing and returns it as a list.
    We are only wanting files with the extension '.spec'.
    Make sure a value of '*' is appended onto the end, as this specifies all files.
    """

    working_dir = "/tmp"

    fc.execute(f'git clone {ret["catalyst_repo"]} {working_dir}/catalyst')
    os.chdir(f"{working_dir}/catalyst")
    fc.execute(f'git checkout {ret["git_branch"]}')
    fc.execute(f"git pull")

    targets = os.listdir(f"{working_dir}/catalyst")
    filtered_targets = [filename for filename in targets if filename.endswith(".spec")]
    filtered_targets.extend(["*", "changed"])

    return filtered_targets


def main():
    VALIDITY: dict = {
        "catalyst_repo": {
            "func": catalyst_repo,
            "mode": "check",
            "default": "https://gitlab.com/xenia-group/catalyst.git",
            "valid_text": "a valid git repository URL for Xenia Linux catalyst",
        },
        "git_branch": {
            "func": git_branch,
            "mode": "execute",
            "default": "unstable",
            "return": True,
        },
        "target": {
            "func": check_target_list,
            "mode": "check",
            "default": "stage4-systemd.spec",
            "return": True,
            "valid_text": "A comma-seperated list of spec files.",
        },
    }

    args = arg_parser()
    config_file = args.config
    interactive = True

    config = {}

    if config_file != "":
        config = toml_parser(config_file)
        interactive = False

    fc.info(
        f"Entering interactive mode. Default values are shown wrapped in square brackets like {fc.Colours.blue}[this]{fc.Colours.endc}. Press enter to accept the default.\n"
        if interactive
        else "Checking config"
    )

    # Let foxcommon parse the config file
    config = fc.parse_config(config, VALIDITY, interactive=interactive)

    environment = Environment(config, args)

    if not args.nobuild:
        environment.start_build()


if __name__ == "__main__":
    main()
