FROM registry.hub.docker.com/gentoo/portage:latest AS portage
FROM registry.hub.docker.com/gentoo/stage3:latest
COPY --from=portage /var/db/repos/gentoo /var/db/repos/gentoo
ENV CONFIG="https://gitlab.com/xenia-group/foxbuild/-/raw/main/build.toml?ref_type=heads"

COPY docker/portage/accept /etc/portage/package.accept_keywords/

RUN mkdir -p /var/foxbuild/output
RUN touch /var/foxbuild/output/Manifest.toml
RUN echo -e 'FEATURES="${FEATURES} binpkg-request-signature"' >> /etc/portage/make.conf
RUN emerge --quiet -g eselect-repository dev-vcs/git tailscale net-fs/sshfs
RUN eselect repository add xenia-overlay git https://gitlab.com/xenia-group/xenia-overlay.git
RUN emaint sync -r xenia-overlay
RUN emerge --quiet -g --autounmask-continue foxbuild 
RUN emerge --depclean
RUN rm -rf /var/cache/distfiles/*

CMD wget $CONFIG -O build.toml && foxbuild -ic build.toml
